FROM golang:latest as build
WORKDIR /app
ADD *.go ./
ADD go.mod ./
RUN go get
RUN go build

FROM ubuntu:latest
RUN apt update && apt install cowsay -y
ENV PATH=$PATH:/usr/games/
COPY --from=build /app/cowserv /app/
WORKDIR /app
EXPOSE 5000
CMD [ "./cowserv" ]
