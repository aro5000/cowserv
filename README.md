# cowserv
A simple HTTP app for demo purposes
```
docker run -it --rm -p 5000:5000 registry.gitlab.com/aro5000/cowserv:latest
```

# Routes
* `/any/path/you/want` - A cow responds with your URL path
* `/load` - Generate CPU load
* `/stop` - Stop the CPU load
* `/fail` - Return a `500` response