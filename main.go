package main

import (
	"bytes"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/exec"
)

var LOADGEN = false

type cowserv struct {
	Url  string
	Port string
}

func main() {
	handler := &cowserv{}
	handler.Port = ":5000"
	fmt.Println("[+] Listening", handler.Port)
	http.ListenAndServe(handler.Port, handler)
}

func (c *cowserv) cowInit(Url string) {
	c.Url = Url
}

func (c *cowserv) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	c.cowInit(r.URL.Path)

	switch c.Url {
	case "/load":
		go generateLoad()
		fmt.Fprintf(w, "[+] Generating Load...\n")
	case "/stop":
		stopLoad()
		fmt.Fprintf(w, "[+] Stopping Load Generation...\n")
	case "/fail":
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "500 - Fail\n")
	case "/name":
		name, _ := os.Hostname()
		fmt.Fprintf(w, name+"\n")
	default:
		cmd := exec.Command("cowsay", "Path is:\n"+c.Url)
		var out bytes.Buffer
		cmd.Stdout = &out
		err := cmd.Run()
		if err != nil {
			fmt.Println("[!] Could not run cowsay command")
			os.Exit(1)
		}
		fmt.Fprintf(w, out.String())

	}
	log.Println(c.Url)
}

func generateLoad() {
	LOADGEN = true
	for i := 0; LOADGEN == true; i++ {
		log.Println(i * i)
	}
	log.Println("[+] Exiting Load Generation Loop!")
}

func stopLoad() {
	LOADGEN = false
}
